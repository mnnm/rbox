import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import VueWindowSize from 'vue-window-size'

Vue.use(VueWindowSize)

new Vue({
  render: h => h(App),
}).$mount('#app')
